/*
 * Programa que imprime un saludo y mi nombre
 */

/**
 *
 * @author Alberto Silva
 */
public class Main {
    // metodo principal
    public static void main(String[] args) {
        // imprime saludo y nombre
        System.out.println("Hola Alberto Silva!");
    }
}
