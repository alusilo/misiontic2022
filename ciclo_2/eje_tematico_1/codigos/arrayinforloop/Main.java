/*
 * ciclo for sobre arreglo o vector
 */

/**
 *
 * @author Alberto Silva
 */
public class Main {

    public static void main(String[] args) {
        // se define tamano de vector o arreglo
        int N = 3;
        // se define arreglo o vector asignando dinamicamente memoria
        int[] numeros = new int[N];
        // se le asigna un valor a cada espacio de memoria
        numeros[0] = 1;
        numeros[1] = 2;
        numeros[2] = 3;
        // ciclo for, inicializado en i=0, hasta que no se cumpla i<5, y va incrementando i de uno en uno (i++)
        for (int i=0; i<N; i++) {
            // se imprime los valores del vector o arreglo
            System.out.println(numeros[i]);
        }
    }
}
