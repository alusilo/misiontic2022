/*
 * Clase principal que recolecta la informacion de tres variables
 * una de ellas es el nombre de un producto, la otra es su precio
 * y la otra es el numero de productos. La informacion debe ser
 * recolectada por medio de entrada estandar. Se debe imprimir cada
 * uno de los datos recolectados.
 */

// importe de modulo Scanner
import java.util.Scanner;

/**
 *
 * @author Alberto Silva
 */
public class Main {

    public static void main(String[] args) {
        // declaracion de variables
        String nombreProducto;
        float valorProducto;
        int cantidadProductos;

        // creacion de una instancia de la clase Scanner
        Scanner in = new Scanner(System.in);
        // recoleccion de la informacion del nombre del producto obtenida
        // por medio de una entrada
        System.out.print("Ingrese el nombre del producto: ");
        nombreProducto = in.nextLine();
        // recoleccion de la informacion del precio del producto obtenida
        // por medio de una entrada
        System.out.print("Ingrese el valor del producto: ");
        valorProducto = in.nextFloat();
        // recoleccion de la informacion de la cantidad de productos obtenida
        // por medio de una entrada
        System.out.print("Ingrese la cantidad de productos: ");
        cantidadProductos = in.nextInt();
        // se imprimen los datos
        System.out.println("El producto es: " + nombreProducto);
        System.out.println("El numero de productos es: " + cantidadProductos);
        System.err.println("El valor por producto es: " + valorProducto);
    }
}
