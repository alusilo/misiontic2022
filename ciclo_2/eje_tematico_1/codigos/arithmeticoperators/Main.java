/*
 * Algunas operaciones aritmeticas en Java
 */

/**
 *
 * @author Alberto Silva
 */
public class Main {

    public static void main(String[] args) {
        // definicion de variables
        float a = 5.5f, b = 7.9f;
        System.out.println("a: " + a);
        // suma de a y b, se almacena en c
        float c = a + b;
        System.out.println("c: " + c);
        // resta de b y c, se almacena en d
        float d = a - b;
        System.out.println("d: " + d);
        // multiplicacion de c y d
        float e = c * d;
        System.out.println("e: " + e);
        // division de c y d
        float f = c / d;
        System.out.println("f: " + f);
        // residuo de la division anterior
        float r = c % d;
        System.out.println("r: " + r);
        // incremento de un valor entero
        int i = 0;
        System.out.println("i (antes de incremento): " + i);
        i++;
        System.out.println("i (antes de decremento): " + i);
        // decremento de un valor entero
        i--;
        System.out.println("i (valor final): " + i);
    }
}
