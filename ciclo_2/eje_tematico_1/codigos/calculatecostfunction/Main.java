/*
 * funciones
 */

import java.util.Scanner;

/**
 *
 * @author Alberto Silva
 */
public class Main {

    public static float calcularCosto(int numeroItems, float precioItem, float descuento) {
        float costoTotal = numeroItems * precioItem;
        float valorDescuento = costoTotal * descuento / 100.0f;
        return costoTotal - valorDescuento;
    }
    public static void main(String[] args) {
        // definicion de variables
        int numeroItems;
        float precioItem, descuento;
        // Objeto Scanner
        Scanner in = new Scanner(System.in);
        // Se recolecta la informacion por entrada estandar
        System.out.print("Ingrese el numero de items: ");
        numeroItems = in.nextInt();
        System.out.print("Ingrese el valor de un item: ");
        precioItem = in.nextFloat();
        System.out.print("Ingrese el descuento a aplicar: ");
        descuento = in.nextFloat();
        // uso de la funcion calcularCosto()
        System.out.println(calcularCosto(numeroItems, precioItem, descuento));
    }
}
