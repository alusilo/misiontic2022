/*
 * Programa que imprime un saludo y mi nombre
 */

// importe de la clase Scanner
import java.util.Scanner;

/**
 *
 * @author Alberto Silva
 */
public class Main {

    public static void main(String[] args) {
        // crea constante, temperatura de referencia
        final float refTemp = 60.0f;
        // se crea el objeto de la clase Scanner para recolectar informacion por entrada estandar
        Scanner in = new Scanner(System.in);
        // variable a evaluar en condicional
        System.out.print("Valor de la temperatura: ");
        float temp = in.nextFloat();
        // se evaluan multiples condiciones
        if (temp > refTemp) {
            System.out.println("Temperatura por encima de " + refTemp);
        } else if (temp < refTemp) {
            System.out.println("Temperatura por debajo de " + refTemp);
        } else if (temp == refTemp) {
            System.out.println("Temperatura igual a " + refTemp);
        }
    }
}
