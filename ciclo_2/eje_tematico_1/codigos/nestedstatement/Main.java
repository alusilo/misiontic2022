/*
 * Se desea comparar el número de items que se desea comprar de determinado producto
 * con el número de items que se tienen de inventario. Si la cantidad de items a
 * comprar es mayor a la inventariada se debe mostrar un mensaje solicitando suplir
 * ese producto. Si hay suficientes productos se debe solicitar empacar los productos
 * en grupos de 4 productos por paquete, si son más de 1 paquete se debe imprimir que
 * se requiere más de un paquete para realizar el pedido.
 */

import java.lang.Math;

/**
 *
 * @author Alberto Silva
 */
public class Main {

    public static void main(String[] args) {
        // define una variable para almacenar el número de items que se desean comprar
        // y otra para almacenar el número de items que se tienen de inventario
        // se declara variable relacionada con el numero de productos por paquete
        int itemsCompra = 23, itemsInventario = 23, productosPorPaquete = 4, numeroPaquetes;
        // se visualiza la información de la compra
        System.out.println("Se desea comprar " + itemsCompra + " items");
        System.out.println("Se tiene en inventario " + itemsInventario + " items.");
        // valor de la variable aprobado si se cumple la condición
        if (itemsCompra > itemsInventario) {
            // imprime aviso solicitando suplir el producto
            System.out.println("Se debe suplir del producto solicitado!");
        } else {
            // se escoge el entero del limite superior
            numeroPaquetes = (int)Math.ceil(itemsCompra / productosPorPaquete);
            if (numeroPaquetes > 1) {
                // imprime que se requieren multiples paquetes
                System.out.println("se requieren " + numeroPaquetes + " paquetes para realizar el pedido");
            }
        }
    }
}
