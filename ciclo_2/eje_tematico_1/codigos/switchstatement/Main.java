/*
 * Ejemplo de switch
 */

import java.util.Scanner;

/**
 *
 * @author Alberto Silva
 */
public class Main {

    public static void main(String[] args) {
        int opcion;
        // lectura por entrada estandar
        Scanner in = new Scanner(System.in);
        // se almacena lo leido en opcion
        System.out.print("digite una opcion (1 o 2): ");
        opcion = in.nextInt();
        switch (opcion){
            case 1:
                System.out.println("Se ejecuta código 1");
                break;
            case 2:
                System.out.println("Se ejecuta código 2");
                break;
            default:
                System.out.println("Se ejecuta código predeterminado");
                break;
        }
    }
}
