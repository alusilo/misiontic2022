/*
 * funciones
 */

/**
 *
 * @author Alberto Silva
 */
public class Main {

    public static double fahrenheit2celcius(double temp) {
        return (temp - 32.0) * 5.0 / 9.0;
    }
    public static void main(String[] args) {
        // uso de la funcion fahrenheit2celcius(), convierte 130 F a C
        System.out.println(fahrenheit2celcius((130.0)));
        // uso de la funcion fahrenheit2celcius(), convierte 90 F a C
        System.out.println(fahrenheit2celcius((90.0)));
        // uso de la funcion fahrenheit2celcius(), convierte 78.5 F a C
        System.out.println(fahrenheit2celcius((78.5)));
    }
}
