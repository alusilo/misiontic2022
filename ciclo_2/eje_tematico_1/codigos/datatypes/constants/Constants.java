/*
 * Clase principal ClaseB
 *
 * Declaracion y definicion de constantes en Java
 */

package constants;

/**
 *
 * @author Alberto Silva
 */
public class Constants {
    // declaracion de constante
    public final float temperaturaAmbiente = 25.0f;
    // constante estatica, es la misma constante en memoria para
    // todas las instancias de esta clase
    public static final int ITERACIONES_MAXIMAS = 50;
}
