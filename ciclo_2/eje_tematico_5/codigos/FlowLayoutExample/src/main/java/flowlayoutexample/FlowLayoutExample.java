/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package flowlayoutexample;

import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author betox
 */
public class FlowLayoutExample {

    public static void main(String[] args) {
        // Ventana con la decoracion predeterminada de Java
        JFrame.setDefaultLookAndFeelDecorated(true);
        // Se crea el frame
        JFrame frame = new JFrame("Flow Layout");
        // se determina el comportamiento del programa al cerrar la ventana
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // se crean los elementos que van a estar dentro del panel
        JButton b1 = new JButton("Boton 1");
        JButton b2 = new JButton("Boton 2");
        JButton b3 = new JButton("Boton 3");
        // se crea el panel
        JPanel panel = new JPanel();
        // Se determina el tipo de plantilla
        panel.setLayout(new FlowLayout(FlowLayout.CENTER));
        // se agregan los elementos al panel
        panel.add(b1);
        panel.add(b2);
        panel.add(b3);
        // se agrega el panel al frame
        frame.add(panel);
        // se determina que el tamaño de la ventana sera el de los elementos
        frame.pack();
        // se hace la ventana visible
        frame.setVisible(true);
    }
}
