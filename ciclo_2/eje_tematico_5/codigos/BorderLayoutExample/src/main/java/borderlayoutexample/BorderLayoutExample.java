/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package borderlayoutexample;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author betox
 */
public class BorderLayoutExample {

    public static void main(String[] args) {
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("Border Layout");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JButton b1 = new JButton("NORTE");
        JButton b2 = new JButton("SUR");
        JButton b3 = new JButton("OESTE");
        JButton b4 = new JButton("ESTE");
        JButton b5 = new JButton("CENTRO");
        
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout(10, 10));
        
        panel.add(b1, BorderLayout.NORTH);
        panel.add(b2, BorderLayout.SOUTH);
        panel.add(b3, BorderLayout.WEST);
        panel.add(b4, BorderLayout.EAST);
        panel.add(b5, BorderLayout.CENTER);
        
        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
    }
}
