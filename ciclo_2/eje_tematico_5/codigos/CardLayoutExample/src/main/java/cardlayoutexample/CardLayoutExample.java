/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package cardlayoutexample;

import java.awt.CardLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author betox
 */
public class CardLayoutExample {

    public static void main(String[] args) {
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("Card layout");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        
        JButton b1 = new JButton("Boton 1");
        JButton b2 = new JButton("Boton 2");
        JButton b3 = new JButton("Boton 3");
        
        JPanel panel = new JPanel();
        panel.setLayout(new CardLayout(10, 10));
        
        panel.add(b1);
        panel.add(b2);
        panel.add(b3);
        
        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
    }
}
