"""
Estructuras de control en Python
Uso de if:
Se desea almacenar la nota de un estudiante y con base a ella
cambiar el valor de una variable que determine cuando un estudiante
aprobó una asignatura.
"""
# define una variable para almacenar la nota
nota = 4.5
# define una variable booleana que indique si con la nota el
# estudiante aprueba la asignatura
aprobado = False
# define una estructura de control condicional que cambie el
# valor de la variable aprobado si se cumple la condición
if nota > 3.0:
    aprobado = True

# imprime si el estudiante aprobó o no
print(f"El estudiante aprobó la asignatura: {aprobado}")
