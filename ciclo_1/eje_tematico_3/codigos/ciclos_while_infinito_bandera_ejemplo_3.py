"""
Estructuras iterativas y variables de control en Python
Bucle while infinito con bandera
el siguiente bucle while simula un sistema cuya funcionalidad
es pedir un número en pantalla que luego es impreso.
"""
# bucle while infinito, se detiene si se digita 0
while True:
    # imprime un aviso relacionado con el uso del sistema
    print("Sistema que imprime un número dado. Digite 0 para salir!")
    # definición de variable que indica un estado, bandera
    opcion = int(input("Digite un número: "))
    # si la variable es diferente de cero imprime el valor de ella
    if opcion != 0:
        print(f"El número es: {opcion}")
    else:
        # si es cero se rompe el ciclo
        break
