"""
Programa para acumular dentro de una lista
"""
# se define variable donde se van a almacenar los datos, en este caso una lista
mercado = []
# se toma por entrada estándar el número de productos
numero_productos = int(input("ingrese numero de productos: "))
# se itera según el número de productos
for i in range(numero_productos):
    # se pide el producto por entrada estándar
    producto = input(f"ingrese producto {i+1}: ")
    # se agrega a la lista el producto mediante el método append()
    mercado.append(producto)

# se imprime la lista de productos
print(f"mercado: {mercado}")