"""
Estructuras iterativas y variables de control en Python
Bucle while infinito con bandera
se desea construir un bucle while que permita simular un sistema en el que se
busca mantener la temperatura en cierto rango de valores
"""
# se definen los siguientes parámetros
# define temperatura actual
temperatura = 25
# define temperatura máxima permitida
temperatura_maxima = 90
# define temperatura mínima permitida
temperatura_minima = 70
# bandera que enciende o apaga el sistema
encendido = True
# bucle while infinito
while True:
    # imprime la temperatura
    print(f'Temperatura: {temperatura:0.2f} C', end="\r")
    # si encendido es verdadero
    if encendido:
        # aumenta la temperatura
        temperatura += 1e-5 # 0.00001
    # si temperatura está por encima de la temperatura máxima
    if temperatura > temperatura_maxima:
        # encendido se hace falso
        encendido = False
    # si la temperatura está por debajo de temperatura mínima
    if temperatura < temperatura_minima:
        # encendido se hace verdadero
        encendido = True
    # la temperatura disminuye a cierta razón siempre
    temperatura -= 1e-6
