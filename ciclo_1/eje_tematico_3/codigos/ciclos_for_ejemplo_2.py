"""
Estructuras iterativas en Python
Imprimir los números del 10 al 20 mediante un ciclo for
"""
# ciclo for
for i in range(10, 21):
    # imprime variable i
    print(i)
