"""
Estructuras iterativas en Python
Imprimir los números del 10 al 20 mediante un ciclo while
"""
# inicialización de variable
i = 10
# ciclo while
while i <= 20:
    # imprime variable i
    print(i)
    # cambia valor de variable i
    # si no se cambia i siempre será 0 generando un ciclo while infinito
    i = i+1
