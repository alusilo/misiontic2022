"""
Programa para obtener la primera palabra de un texto
"""
# variable que almacena un texto
texto = "Hola mundo como estas"

# define parametro para almacenar la palabra
palabra = ""

# ciclo for para iterar en el texto caracter por caracter
for c in texto:
    # condición que verifica cuando el caracter es un espacio vacío
    if c == " ":
        # si el caracter es un espacio en vacío se rompe el ciclo for
        break
    # se acumulan los caracteres en palabra
    palabra = palabra + c

# se imprime la primera palabra del texto
print(f"La primera palabra es: {palabra}")
