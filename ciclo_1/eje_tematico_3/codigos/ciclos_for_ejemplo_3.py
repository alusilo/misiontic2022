"""
Estructuras iterativas en Python
Imprimir los siguientes números mediante un ciclo for:
5, -2, 4, 1, 0, 7
"""
# definición de lista
numeros = [5, -2, 4, 1, 0, 7]
# ciclo for
for numero in numeros:
    # imprime variable numero
    print(numero)
