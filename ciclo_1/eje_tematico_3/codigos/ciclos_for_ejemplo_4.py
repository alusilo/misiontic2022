"""
Estructuras iterativas en Python
Imprimir las vocales mediante un ciclo for
"""
# definición de una cadena de caracteres
vocales = "aeiou"
# ciclo for
for vocal in vocales:
    # imprime variable vocal
    print(vocal)
