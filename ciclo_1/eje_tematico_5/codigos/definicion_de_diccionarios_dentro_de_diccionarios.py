"""
Definición de diccionarios en Python
"""
# diccionario materias
rotacion = {
    "asignatura": "Rotación electiva",
    "tipo": "electiva",
    "creditos": 10,
    "profesor": "Sergio Andrade"
}
dermatologia = {
    "asignatura": "Dermatología II",
    "tipo": "carrera",
    "creditos": 12,
    "profesor": "Maria Sierra"
}
# diccionario estudiante
estudiante = {
    "nombre": "Laura",
    "apellido": "Perez",
    "correo": "laura.perez@correo.co",
    "edad": 22,
    "grupo": "B1",
    "asignaturas": [rotacion, dermatologia],
    "notas": [4.1, 4.5]
}
