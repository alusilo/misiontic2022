"""
Definición de diccionarios en Python
"""
# diccionario estudiante
estudiante = {
    "nombre": "Laura",
    "apellido": "Perez",
    "correo": "laura.perez@correo.co",
    "edad": 22,
    "grupo": "B1",
    "asignaturas": ["Rotación electiva", "Dermatología II", "Pediatría IV", "Cirugía I"],
    "notas": [4.1, 4.5, 4.4, 4.8]
}
