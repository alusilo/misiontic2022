"""
Código para calcular si una persona es mayor de edad usando Python
"""
# definición del año actual
anio_actual = 2022
# definición del año de nacimiento por entrada estándar
anio_nacimiento = int(input("Año de nacimiento: "))
# cálculo de la edad
edad = anio_actual - anio_nacimiento
# determinar si la edad es mayor o igual a 18 años
es_mayor = edad >= 18
# imprime la edad
print(f"La edad es: {edad}")
# imprime el resultado de la comparación si edad es mayor a 18
print(f"Es mayor de edad: {es_mayor}")
