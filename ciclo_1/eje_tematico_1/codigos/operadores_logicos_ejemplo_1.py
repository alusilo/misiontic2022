"""
Operadores lógicos en Python
Operador and
"""
# almacenamiento de la operación lógica True and True
condicion_a = True and True
# impresión del resultado de la operación anterior
print(condicion_a)
# almacenamiento de la operación lógica True and False
condicion_b = True and False
# impresión del resultado de la operación anterior
print(condicion_b)
# almacenamiento de la operación lógica False and False
condicion_c = False and False
# impresión del resultado de la operación anterior
print(condicion_c)
# almacenamiento de la operación lógica False and True
condicion_d = False and True
# impresión del resultado de la operación anterior
print(condicion_d)
