"""
Operadores aritméticos en Python
Operadores para división (/) y multiplicación (*)
"""
# Define variable entera o flotante
numero_a = 10
# Define variable entera o flotante
numero_b = 3
# Suma de dos variables que pueden ser enteras o flotante o combinadas
numero_c = numero_a + numero_b
# Resta de dos variables que pueden ser enteras o flotante o combinadas
numero_d = numero_a – numero_b
# Define número a partir de la división de dos de ellos
numero_e = numero_c / numero_d
# Imprime el resultado de la division
print(numero_e)
# Define número a partir de la multiplicación de dos de ellos
numero_f = numero_c * numero_d
# Imprime el resultado de la multiplicación
print(numero_f)
