"""
Operadores de comparación en Python
Comparación entre cadena de caracteres
"""
# Definición de cadena de caracteres
palabra_a = "hola"
# Definición de cadena de caracteres
palabra_b = "chao"
# Comparación entre cadena de caracteres
# El operador mayor que compara de acuerdo al orden alfabético
# de acuerdo al orden ABC...XYZabc...xyz, A < a, F > C, p > h
# En este ejemplo h > c por loque el resultado es True
print(palabra_a > palabra_b)
# Comparación de dos palabras, si son iguales el resultado es True,
# de lo contrario resulta False
print(palabra_a == palabra_b)
