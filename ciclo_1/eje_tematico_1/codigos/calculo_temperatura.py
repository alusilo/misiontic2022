"""
Código para convertir grados Fahrenhet en Celcius
"""
# Definición de variable para almacenar el valor de la temperatura en Fahrenheit
temperatura_fahrenheit = float(input("Temperatura en Farenheit: "))
# Cálculo de la temperatura en Celcius
temperatura_celcius = (temperatura_fahrenheit - 32) * 5 / 9
# Imprime el valor de la temperatura en Fahrenheit y en Celcius
print(f"{temperatura_fahrenheit} grados Fahrenheit equivalen a {temperatura_celcius} grados Celcius")
