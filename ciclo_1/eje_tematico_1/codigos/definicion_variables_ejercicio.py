"""
Ejercicio de definición de variables en Python
"""
# Define nombre de usuario como cadena de caracteres
usuario = "Jose Suarez"
# Define información de pedido de usuario como cadena de caracteres
informacion_pedido = "Tenis adidas Duramo SL 2.0 x 1 par"
# Define dirección como cadena de caracteres
direccion = "Calle 35 #38-20 AP 302"
# define total a pagar como entero
total_pago = 269990
