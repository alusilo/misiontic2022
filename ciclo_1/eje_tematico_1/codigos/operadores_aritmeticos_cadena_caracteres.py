"""
Operadores aritméticos en cadena de caracteres en Python
Operadores (+) y (*)
"""
# Concatenación de cadena de caracteres usando operador (+)
# Define tipo de dato cadena de caracteres para el nombre
nombre = "Juliana"
# Define tipo de dato cadena de caracteres para el apellido
apellido = "Ríos"
# Define una variable para almacenar el nombre completo usando
# el operador (+) para concatenar dos cadenas de caracteres
nombre_completo = nombre + " " + apellido
# Imprime el nombre completo

# Uso del operador (*) para multiplicar cadena de caracteres con un entero
cadena = "Hola" * 3
# Imprime la variable cadena
print(cadena)
