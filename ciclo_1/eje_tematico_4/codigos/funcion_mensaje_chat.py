"""
Función que imprime un mensaje y retorna una fecha y hora

Desarrolle una función que reciba un nombre y un mensaje e imprima de la
siguiente manera "Nombre: Mensaje", por ejemplo:

"Luis: Hola!"

y retorne la fecha y hora.
"""
from datetime import datetime


# definición de la función
def mensaje_chat(nombre, mensaje):
    print(f"{nombre}: {mensaje}")
    return datetime.now()


# uso de la función
mensaje_chat("Luis", "Hola!")
