"""
Función con que cuenta caracteres en cadenas de caracteres que pertenecen a una lista.

Dada una lista, cuyos items son cadenas de caracteres, cree una función que permita
iterar sobre dicha lista y cuente de cada cadena de caracteres el número de vocales,
solamente si dicho item es diferente de None. Se debe retornar una lista cuyos items
son números enteros asociados con el número de vocales dentro de cada cadena.

lista = ["Hola", "Carretera", "SPD", None, "Librería"]
resultado = [2, 4, 0, 4]
"""


# definición de la función


# uso de la función
